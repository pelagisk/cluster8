module Main exposing ( main )
import Texts
import Computer as C
import Zine as Z
import Dict exposing ( Dict )
import Set exposing ( Set )
import Array
import Browser
import Browser.Navigation as Nav
import Browser.Dom as Dom
import Url
import Element exposing (..)
import Element.Border as Border
import Element.Font as Font
import Element.Background as Background
import Html exposing ( Html, div, img, a )
import Markdown
import Browser.Events as E
import Html.Attributes as H
import Json.Decode as D


-- MODEL

type alias Flags =
    { width : Int
    , height : Int
    }

type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , device : Device
    , computer : C.Computer
    , zineModel : Z.ZineModel
    }

init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init () url key =
    let
        -- TODO these two could/should be set by flags, see:
        -- https://github.com/opsb/cv-elm/blob/master/src/index.js
        width = 800
        height = 600
        model =
            { url = url
            , key = key
            , device = Element.classifyDevice { width = 800, height = 600 }
            , computer = C.initComputer width height
            , zineModel = Z.initZineModel
            }
    in
        ( model, Cmd.none )

-- UPDATE

type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | WindowResized Int Int
    | KeyChanged Bool String
    | MouseMove Float Float
    | MouseButton Bool

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- manually clicked URL
        LinkClicked urlRequest ->
          case urlRequest of
            Browser.Internal url ->
              ( model, Nav.pushUrl model.key (Url.toString url) )
            Browser.External href ->
              ( model, Nav.load href )
        -- updated URL in nav bar
        UrlChanged url ->
          ( { model | url = url }
          , Cmd.none
          )
        -- window resized
        WindowResized width height ->
            let
                device = Element.classifyDevice { width = width
                                                , height = height }
                w = toFloat width
                h = toFloat height
                updatedScreen = C.toScreen w h
                computer = model.computer
                updatedComputer = { computer | screen = updatedScreen }
            in
                ( { model | device = device, computer = updatedComputer}
                , Cmd.none
                )
        -- for zine page, track keyboard and mouse
        -- TODO here, could updateZine be moved to Zine module?
        otherMsg -> case model.url.fragment of
            Just "zine" -> updateZine otherMsg model
            _ -> updateZine otherMsg model -- ( model, Cmd.none)

updateZine : Msg -> Model -> ( Model, Cmd Msg )
updateZine msg model =
    case msg of
        -- key pressed - TODO update zoom
        KeyChanged isDown key ->
            let
                computer = model.computer
                updatedKeyboard = C.updateKeyboard isDown key computer.keyboard
                updatedComputer = { computer | keyboard = updatedKeyboard }
                zineModel = model.zineModel
                newZineModel = if isDown == True then
                                   case key of
                                       "z" -> { zineModel | zoom = min (zineModel.zoom + 1) 1000 }
                                       "x" -> { zineModel | zoom = max (zineModel.zoom - 1) 0 }
                                       _ -> zineModel
                               else
                                   zineModel
            in
                ( { model | computer = updatedComputer, zineModel = zineModel }
                , Cmd.none
                )
        -- mouse moved - move pieces
        MouseMove pageX pageY ->
            let
                x = pageX -- model.screen.left + pageX
                y = pageY -- model.screen.top - pageY
                computer = model.computer
                updatedMouse = C.mouseMove x y model.computer.mouse
                updatedComputer = { computer | mouse = updatedMouse }
                zineModel = model.zineModel
                selection = zineModel.selection
                newZineModel = if selection == -1 then
                                   zineModel
                               else
                                   Z.updatePosition selection x y zineModel
            in
                ( { model | computer = updatedComputer, zineModel = newZineModel }
                , Cmd.none
                )
        -- mouse button up/down - update selection
        MouseButton isDown ->
            let
                computer = model.computer
                updatedMouse = C.mouseDown isDown computer.mouse
                updatedComputer = { computer | mouse = updatedMouse }
                zineModel = model.zineModel
                newZineModel = case isDown of
                    True -> Z.updateSelection updatedComputer zineModel
                    False -> Z.resetSelection zineModel
            in
                ( { model | computer = updatedComputer, zineModel = newZineModel }
                , Cmd.none
                )
        _ -> ( model , Cmd.none)


subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.batch
    [ E.onResize <| \width height -> WindowResized width height
    , E.onKeyUp (D.map (KeyChanged False) (D.field "key" D.string))
    , E.onKeyDown (D.map (KeyChanged True) (D.field "key" D.string))
    , E.onMouseDown (D.succeed (MouseButton True))
    , E.onMouseUp (D.succeed (MouseButton False))
    , E.onMouseMove (D.map2 (MouseMove) (D.field "pageX" D.float) (D.field "pageY" D.float))
    ]


-- VIEW

view : Model -> Browser.Document Msg
view model =
  { title = titleText
  , body = [ viewBody model ]
  }

viewBody : Model -> Html msg
viewBody model =
    case model.device.class of
        Phone -> viewBodyCompact model
        _ -> viewBodyFull model

viewBodyCompact : Model -> Html msg
viewBodyCompact model =
    let
        fragment = Maybe.withDefault "" model.url.fragment
        compactBody =
            if (fragment == "") || (fragment == "menu") then
                [ logo
                , expandMenuLink
                , content model
                , instagram
                ]
            else
                [ logo
                , expandMenuLink
                , pageTitle model
                , content model
                ]
    in
        layout
            [ width fill
            , height fill
            , Background.color color.black
            , Font.color color.white
            -- , Font.italic
            , Font.family
                [ Font.typeface "Avenir Next Condensed"
                , Font.sansSerif
                ]
            ] <|
            column
                [ width fill
                , height fill ]
                compactBody

viewBodyFull : Model -> Html msg
viewBodyFull model =
    layout
        [ width fill
        , height fill
        , Background.color color.black
        , Font.color color.white
        -- , Font.italic
        , Font.family
            [ Font.typeface "Avenir Next Condensed"
            , Font.sansSerif
            ]
        ] <|
        row
            [ width fill
            , height fill
            ]
            -- left: logo and menu
            [ column
                [ width <| px 240
                , height fill
                ]
                [ logo
                , menu model
                , instagram
                ]
            -- right: page title and content
            , column
                [ width fill
                , height fill
                ]
                [ pageTitle model
                , content model
                ]
            ]

logo : Element msg
logo =
    row
        [ width fill
        , height <| px 80
        , padding 10
        ]
        [ link
            [ -- Font.size 60
            ]
            { url = "#"
            , label = image [ width <| px 160 ] { src = "../public/img/cluster8.png"
                            , description = "logo"}
            }
        ]

instagram : Element msg
instagram =
    row
        [ width fill
        , height <| px 80
        , padding 10
        -- , alignTop
        ]
        [ link
            [ Font.size 60
            ]
            { url = "https://www.instagram.com/cluster.8/"
            , label = image [ width <| px 40 ] { src = "../public/img/instagram.png"
                            , description = "instagram"}
            }
        ]

expandMenuLink : Element msg
expandMenuLink =
    row
        [ width fill
        -- , height <| px 10
        , padding 10
        ]
        [ link
            [ Font.size 30
            , Font.underline
            ]
            { url = "#menu"
            , label = text "Menu ↓"
            }
        ]

menu : Model -> Element msg
menu model =
    let
        onlyUncategorized (url, data) = Set.isEmpty data.tags
    in
        menuFiltered onlyUncategorized model

tagMenu : String -> Model -> Element msg
tagMenu t model =
    let
        hasTag (url, data) = Set.member t data.tags
    in
        menuFiltered hasTag model

menuFiltered : ((String, PageData) -> Bool) -> Model -> Element msg
menuFiltered f model =
    let
        makeLink (url, data) =
            link
                [ alignTop
                , Font.underline
                , Font.italic
                -- , Font.variant Font.smallCaps
                ]
                { url = "#" ++ url
                , label = text (String.toUpper data.title)
                }
    in
        column
            [ width fill
            , padding 10
            , spacing 10
            , alignTop
            -- , Background.color color.darkCharcoal
            ]
            <| List.map makeLink <| List.filter f <| Dict.toList pagesData

pageTitle :  Model -> Element msg
pageTitle model =
    let
        getPageTitle f =
            case ( Dict.get f pagesData ) of
                Just data -> data.title
                Nothing -> ""
        title = case model.url.fragment of
            Just f -> getPageTitle f
            Nothing -> ""
    in
        row
            [ width fill
            , height <| px 80
            , padding 10
            -- , Background.color color.darkCharcoal
            ]
            [ el
                [ Font.size 60
                , Font.italic
                ]
                <| text (String.toUpper title)
            ]

content :  Model -> Element msg
content model =
    case model.url.fragment of
        -- from mobile, the menu is another view
        Just "menu" -> case model.device.class of
                           Phone -> menu model
                           _ -> indexContent model
        -- the zine content is special
        Just "zine" -> zineContent model
        -- otherwise, show a regular Markdown page
        Just fragment -> case (Dict.get fragment pagesData) of
                             Just data -> pageContent fragment data model
                             _ -> indexContent model
        _ -> indexContent model

pageContent : String -> PageData -> Model -> Element msg
pageContent fragment data model =
    column
        [ width fill
        , height fill
        ]
        [ tagMenu fragment model
        , textColumn
            [ width fill
            , height fill
            , padding 10
            , scrollbarY
            ]
            [ paragraph [] [ fromMarkdown data.text ]
            ]
        ]

indexContent : Model -> Element msg
indexContent model =
    pageContent "" { title = ""
                   , text = ""
                   , tags = Set.empty
                   } model

zineContent : Model -> Element msg
zineContent model =
    pageContent "" { title = "Zine"
                   , text = "Under construction!"
                   , tags = Set.empty
                   } model

-- zineContent : Model -> Element msg
-- zineContent model =
--     let
--         widthText = "width: " ++ (String.fromFloat model.computer.screen.width)
--         posText = "<br> x: " ++ (String.fromFloat model.computer.mouse.x)
--         mouseText = "<br> mouse down: " ++ (if model.computer.mouse.down then "yes" else "no")
--         zineSelText = "<br> zine selection: " ++ (String.fromInt model.zineModel.selection)
--         f (x, y) = "(" ++ (String.fromFloat x) ++ ", " ++ (String.fromFloat y) ++ ", )"
--         positions = "<br> " ++ Array.foldl (++) "" (Array.map f model.zineModel.positions)
--         txt = widthText ++ posText ++ mouseText ++ zineSelText ++ positions
--     in
--         column
--             [ width fill
--             , height fill
--             ]
--             <| [ textColumn
--                 [ width fill
--                 , height fill
--                 , padding 10
--                 , scrollbarY
--                 ]
--                 [ paragraph [] [ text puzzleText ]
--                 ]
--             ] ++ pieces model

puzzleText =
  "z: zoom in, x: zoom out, drag/drop to move pieces, move around with arrows"


pieces model =
    let
        url i = "../public/img/" ++ (String.fromInt i) ++ ".png"
        f i position = placeImage model (url i) position
    in
        (Array.indexedMap f model.zineModel.positions) |> Array.toList


placeImage model url (x, y) =
    let
        (xz, yz) = Z.getPos model.zineModel (x, y)
        (w, h) = Z.getScaled model.zineModel Z.initialImageSize
        (xc, yc) = (xz - w/2, yz - h/2)
    in
        image
            [ moveRight xc
            , moveDown yc
            , width (px (round w))
            , height (px (round h))
            ]
            { src = url
            , description = url
            }
        -- html <| div
        --   [ H.class "puzzle-div"
        --   , H.style "left" <| inPixels xc
        --   , H.style "top" <| inPixels yc
        --   ]
        --   [
        --     img
        --       [ H.src url
        --       , H.class "puzzle-img"
        --       , H.style "width" <| inPixels w
        --       , H.style "height" <| inPixels h
        --       ]
        --       []
        --   ]

inPixels x =
  (String.fromFloat x) ++ "px"

type alias PageData =
    { tags : Set String
    , text : String
    , title : String
    }

pagesData : Dict String PageData
pagesData =
    Dict.fromList
        [ ( "about", { title = "About Cluster 8", text = Texts.about, tags = Set.empty } )
        , ( "archive", { title = "Sandberg Archive", text = Texts.archive, tags = Set.empty } )
        , ( "nos", { title = "Gallery NOS", text = Texts.nos, tags = Set.empty } )
        , ( "zine", { title = "Zine", text = "", tags = Set.empty } )
        , ( "writing", { title = "Writing", text = "", tags = Set.empty } )
        , ( "otto", { title = "Thinking Pollen - A method to identify the past - Otto Ruin", text = Texts.otto, tags = Set.fromList [ "writing" ] } )
        , ( "axel", { title = "Data and ruins - Axel Gagge", text = Texts.axel, tags = Set.fromList [ "writing" ] } )
        , ( "isabelle", { title = "Existing Beyond Ourselves - Isabelle Ribe", text = Texts.isabelle, tags = Set.fromList [ "writing" ] } )
        , ( "lior", { title = "Ode to Sandberg - Lior Nønne Malue Hansen", text = Texts.lior, tags = Set.fromList [ "writing" ] } )
        , ( "concept", { title = "Concepts", text = "", tags = Set.empty } )
        , ( "palynology", { title = "Palynology", text = Texts.palynology, tags = Set.fromList [ "concept" ] } )
        , ( "ruins", { title = "Ruins", text = Texts.ruins, tags = Set.fromList [ "concept" ] } )
        ]

titleText = "cluster ∞"

sampleText =
    """
But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
"""


sampleText2 =
    """
No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.
"""

color =
    { blue = rgb255 0x72 0x9F 0xCF
    , darkCharcoal = rgb255 0x2E 0x34 0x36
    , lightBlue = rgb255 0xC5 0xE8 0xF7
    , lightGrey = rgb255 0xE0 0xE0 0xE0
    , white = rgb255 0xFF 0xFF 0xFF
    , black = rgb255 0x00 0x00 0x00
    }

markdownOptions =
    { githubFlavored = Just { tables = False, breaks = False }
    , defaultHighlighting = Nothing
    , sanitize = False
    , smartypants = True
    }

fromMarkdown text = html <| Markdown.toHtmlWith markdownOptions [] text

-- MAIN


main : Program () Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }
