module Main exposing (main)

import Array
import Browser
import Browser.Dom as Dom
import Browser.Events as E
import Browser.Navigation as Nav
import Dict exposing (Dict)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html exposing (Html)
import Html.Attributes as H
import Json.Decode as D
import Markdown
import Set exposing (Set)
import Texts
import Url



-- MODEL


type alias Flags =
    { width : Int
    , height : Int
    }


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , device : Device
    }


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        model =
            { url = url
            , key = key

            -- TODO not working on mobile
            , device =
                Element.classifyDevice
                    { width = flags.width
                    , height = flags.height
                    }
            }
    in
    ( model, Cmd.none )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | WindowResized Int Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- manually clicked URL
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    let
                        urlString =
                            Url.toString url
                    in
                    case urlString of
                        "https://cluster8.org/puzzle.html" ->
                            ( model, Nav.load urlString )

                        _ ->
                            ( model, Nav.pushUrl model.key urlString )

                Browser.External href ->
                    ( model, Nav.load href )

        -- updated URL in nav bar
        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )

        -- window resized
        WindowResized width height ->
            let
                device =
                    Element.classifyDevice
                        { width = width
                        , height = height
                        }
            in
            ( { model | device = device }
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ E.onResize <| \width height -> WindowResized width height
        ]



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = titleText
    , body = [ viewBody model ]
    }


viewBody : Model -> Html msg
viewBody model =
    case model.device.class of
        Phone ->
            viewBodyCompact model

        _ ->
            viewBodyFull model


viewBodyCompact : Model -> Html msg
viewBodyCompact model =
    let
        fragment =
            Maybe.withDefault "" model.url.fragment

        compactBody =
            if (fragment == "") || (fragment == "menu") then
                [ logo
                , expandMenuLink
                , content model
                , instagram
                ]

            else
                [ logo
                , expandMenuLink
                , pageTitle model
                , content model
                ]
    in
    layout
        [ width fill
        , height fill
        , Background.color color.black
        , Font.color color.white
        , Font.family
            [ Font.typeface "Avenir Next Condensed"
            , Font.external
                { url = "http://fonts.cdnfonts.com/css/avenir-next-cyr"
                , name = "Avenir Next Cyr"
                }
            , Font.sansSerif
            ]
        ]
    <|
        column
            [ width fill
            , height fill
            ]
            compactBody


viewBodyFull : Model -> Html msg
viewBodyFull model =
    layout
        [ width fill
        , height fill
        , Background.color color.black
        , Font.color color.white
        , Font.family
            [ Font.typeface "Avenir Next Condensed"
            , Font.sansSerif
            ]
        ]
    <|
        row
            [ width fill
            , height fill
            ]
            -- left: logo and menu
            [ column
                [ width <| px 240
                , height fill
                ]
                [ logo
                , menu model
                , instagram
                ]

            -- right: page title and content
            , column
                [ width fill
                , height fill
                ]
                [ pageTitle model
                , content model
                ]
            ]


logo : Element msg
logo =
    row
        [ width fill
        , height <| px 80
        , padding 10
        ]
        [ link
            []
            { url = "#"
            , label =
                image
                    [ width <| px 160 ]
                    { src = "img/cluster8.png"
                    , description = "logo"
                    }
            }
        ]


instagram : Element msg
instagram =
    row
        [ width fill
        , height <| px 80
        , padding 10
        ]
        [ link
            [ Font.size 60
            ]
            { url = "https://www.instagram.com/cluster.8/"
            , label =
                image
                    [ width <| px 40 ]
                    { src = "img/instagram.png"
                    , description = "instagram"
                    }
            }
        ]


expandMenuLink : Element msg
expandMenuLink =
    row
        [ width fill
        , padding 10
        ]
        [ link
            [ Font.size 30
            , Font.underline
            ]
            { url = "#menu"
            , label = text "Menu ↓"
            }
        ]


menu : Model -> Element msg
menu model =
    let
        onlyUncategorized ( url, data ) =
            Set.isEmpty data.tags

        pageLinks =
            pagesFiltered onlyUncategorized model

        zineLink =
            link
                [ alignTop
                , Font.underline
                , Font.italic
                ]
                { url = "https://cluster8.org/puzzle.html"
                , label = text "ZINE"
                }

        links =
            pageLinks

        --  ++ [ zineLink ]
    in
    menuView links


tagMenu : String -> Model -> Element msg
tagMenu t model =
    let
        hasTag ( url, data ) =
            Set.member t data.tags

        links =
            pagesFiltered hasTag model
    in
    menuView links


menuView : List (Element msg) -> Element msg
menuView links =
    column
        [ width fill
        , padding 10
        , spacing 10
        , alignTop
        ]
    <|
        links


pagesFiltered : (( String, PageData ) -> Bool) -> Model -> List (Element msg)
pagesFiltered f model =
    let
        makeLink ( url, data ) =
            link
                [ alignTop
                , Font.underline
                , Font.italic
                ]
                { url = "#" ++ url
                , label = text (String.toUpper data.title)
                }
    in
    List.map makeLink <| List.filter f <| Dict.toList pagesData


pageTitle : Model -> Element msg
pageTitle model =
    let
        getPageTitle f =
            case Dict.get f pagesData of
                Just data ->
                    data.title

                Nothing ->
                    ""

        title =
            case model.url.fragment of
                Just f ->
                    getPageTitle f

                Nothing ->
                    ""
    in
    row
        [ width fill
        , height <| px 80
        , padding 10
        ]
        [ el
            [ Font.size 60
            , Font.italic
            ]
          <|
            text (String.toUpper title)
        ]


content : Model -> Element msg
content model =
    case model.url.fragment of
        -- from mobile, the menu is another view
        Just "menu" ->
            case model.device.class of
                Phone ->
                    menu model

                _ ->
                    emptyContent model

        -- otherwise, show a regular Markdown page
        Just fragment ->
            case Dict.get fragment pagesData of
                Just data ->
                    pageContent fragment data model

                _ ->
                    pageContent "about" aboutData model

        _ ->
            pageContent "about" aboutData model


pageContent : String -> PageData -> Model -> Element msg
pageContent fragment data model =
    column
        [ width fill
        , height fill
        ]
        [ tagMenu fragment model
        , textColumn
            [ width fill
            , height fill
            , padding 10
            , scrollbarY
            ]
            [ paragraph [] [ fromMarkdown data.text ]
            ]
        ]


emptyContent : Model -> Element msg
emptyContent model =
    pageContent ""
        { title = ""
        , text = ""
        , tags = Set.empty
        }
        model


type alias PageData =
    { tags : Set String
    , text : String
    , title : String
    }


aboutData : PageData
aboutData =
    { title = "About Cluster 8", text = Texts.about, tags = Set.empty }


pagesData : Dict String PageData
pagesData =
    Dict.fromList
        [ ( "about", aboutData )
        -- , ( "archive", { title = "Sandberg Archive", text = Texts.archive, tags = Set.empty } )
        -- , ( "exhibitions", { title = "Exhibitions", text = Texts.exhibition, tags = Set.empty } )
        , ( "writing", { title = "Writing", text = "", tags = Set.empty } )
        , ( "otto", { title = "Thinking Pollen - A method to identify the past - Otto Ruin", text = Texts.otto, tags = Set.fromList [ "writing" ] } )
        , ( "axel", { title = "Data and ruins - Axel Gagge", text = Texts.axel, tags = Set.fromList [ "writing" ] } )
        , ( "isabelle", { title = "Existing Beyond Ourselves - Isabelle Ribe", text = Texts.isabelle, tags = Set.fromList [ "writing" ] } )
        , ( "lior", { title = "Ode to Sandberg - Lior Nønne Malue Hansen", text = Texts.lior, tags = Set.fromList [ "writing" ] } )

        -- , ( "concept", { title = "Concepts", text = "", tags = Set.empty } )
        -- , ( "palynology", { title = "Palynology", text = Texts.palynology, tags = Set.fromList [ "concept" ] } )
        -- , ( "ruins", { title = "Ruins", text = Texts.ruins, tags = Set.fromList [ "concept" ] } )
        ]


titleText =
    "cluster ∞"


color =
    { blue = rgb255 0x72 0x9F 0xCF
    , darkCharcoal = rgb255 0x2E 0x34 0x36
    , lightBlue = rgb255 0xC5 0xE8 0xF7
    , lightGrey = rgb255 0xE0 0xE0 0xE0
    , white = rgb255 0xFF 0xFF 0xFF
    , black = rgb255 0x00 0x00 0x00
    }


markdownOptions =
    { githubFlavored = Just { tables = False, breaks = False }
    , defaultHighlighting = Nothing
    , sanitize = False
    , smartypants = True
    }


fromMarkdown text =
    html <| Markdown.toHtmlWith markdownOptions [] text



-- MAIN


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }
