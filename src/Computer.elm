module Computer exposing (..)
import Set


-- SCREEN HELPERS

type alias Screen =
    { width : Float
    , height : Float
    , top : Float
    , left : Float
    , right : Float
    , bottom : Float
    }


toScreen : Float -> Float -> Screen
toScreen width height =
    { width = width
    , height = height
    , top = height / 2
    , left = -width / 2
    , right = width / 2
    , bottom = -height / 2
    }


-- MOUSE

type alias Mouse =
    { x : Float
    , y : Float
    , down : Bool
    , click : Bool
    }

mouseClick : Bool -> Mouse -> Mouse
mouseClick bool mouse =
    { mouse | click = bool }


mouseDown : Bool -> Mouse -> Mouse
mouseDown bool mouse =
    { mouse | down = bool }


mouseMove : Float -> Float -> Mouse -> Mouse
mouseMove x y mouse =
    { mouse | x = x, y = y }



-- KEYBOARD HELPERS

type alias Keyboard =
    { up : Bool
    , down : Bool
    , left : Bool
    , right : Bool
    , space : Bool
    , enter : Bool
    , shift : Bool
    , backspace : Bool
    , keys : Set.Set String
    }

emptyKeyboard : Keyboard
emptyKeyboard =
    { up = False
    , down = False
    , left = False
    , right = False
    , space = False
    , enter = False
    , shift = False
    , backspace = False
    , keys = Set.empty
    }


updateKeyboard : Bool -> String -> Keyboard -> Keyboard
updateKeyboard isDown key keyboard =
    let
        keys =
            if isDown then
                Set.insert key keyboard.keys
            else
                Set.remove key keyboard.keys
    in
        case key of
            " "          -> { keyboard | keys = keys, space = isDown }
            "Enter"      -> { keyboard | keys = keys, enter = isDown }
            "Shift"      -> { keyboard | keys = keys, shift = isDown }
            "Backspace"  -> { keyboard | keys = keys, backspace = isDown }
            "ArrowUp"    -> { keyboard | keys = keys, up = isDown }
            "ArrowDown"  -> { keyboard | keys = keys, down = isDown }
            "ArrowLeft"  -> { keyboard | keys = keys, left = isDown }
            "ArrowRight" -> { keyboard | keys = keys, right = isDown }
            _            -> { keyboard | keys = keys }

toX : Keyboard -> Float
toX keyboard =
    (if keyboard.right then 1 else 0) - (if keyboard.left then 1 else 0)

toY : Keyboard -> Float
toY keyboard =
    (if keyboard.up then 1 else 0) - (if keyboard.down then 1 else 0)


-- COMPUTER

type alias Computer =
    { mouse : Mouse
    , keyboard : Keyboard
    , screen : Screen
    }

initComputer : Float -> Float -> Computer
initComputer width height =
    { mouse = Mouse 0 0 False False
    , keyboard = emptyKeyboard
    , screen = toScreen width height
    }
