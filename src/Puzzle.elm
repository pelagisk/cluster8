module Puzzle exposing (main)

import Array
import Browser
import Browser.Events as E
import Browser.Navigation as Nav
import Computer as C
import Html exposing (..)
import Html.Attributes as H
import Json.Decode as D
import Set exposing (Set)
import Url


type alias PuzzleModel =
    { zoom : Float
    , center : ( Float, Float ) -- delete this?
    , selection : Int
    , selectedAt : ( Float, Float )
    , positions : Array.Array ( Float, Float )
    }


initPuzzleModel : PuzzleModel
initPuzzleModel =
    { zoom = 0
    , center = ( 0, 0 )
    , selection = -1
    , selectedAt = ( 0, 0 )
    , positions = initialPositions
    }


initialPositions =
    Array.fromList
        [ ( 400, 240 )
        , ( 210, 490 )
        , ( 100, 600 )
        , ( 600, 450 )
        , ( 350, 550 )
        , ( 230, 300 )
        , ( 750, 230 )
        , ( 750, 520 )
        , ( 110, 270 )
        , ( 600, 240 )
        , ( 470, 500 )
        , ( 650, 670 )
        ]


initialImageSize =
    ( 105, 149 )


titleText =
    "puzzle ∞"



-- MODEL


type alias Flags =
    { width : Int
    , height : Int
    }


type alias Model =
    { computer : C.Computer
    , puzzleModel : PuzzleModel
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init () url key =
    let
        -- TODO these two could/should be set by flags, see:
        -- https://github.com/opsb/cv-elm/blob/master/src/index.js
        width =
            800

        height =
            600

        model =
            { computer = C.initComputer width height
            , puzzleModel = initPuzzleModel
            }
    in
    ( model, Cmd.none )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | WindowResized Int Int
    | KeyChanged Bool String
    | MouseMove Float Float
    | MouseButton Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- window resized
        WindowResized width height ->
            let
                w =
                    toFloat width

                h =
                    toFloat height

                updatedScreen =
                    C.toScreen w h

                computer =
                    model.computer

                updatedComputer =
                    { computer | screen = updatedScreen }
            in
            ( { model | computer = updatedComputer }
            , Cmd.none
            )

        -- key pressed - TODO update zoom
        KeyChanged isDown key ->
            let
                computer =
                    model.computer

                updatedKeyboard =
                    C.updateKeyboard isDown key computer.keyboard

                updatedComputer =
                    { computer | keyboard = updatedKeyboard }

                puzzleModel =
                    model.puzzleModel

                newPuzzleModel =
                    if isDown == True then
                        case key of
                            "z" ->
                                { puzzleModel | zoom = min (puzzleModel.zoom + 1) 1000 }

                            "x" ->
                                { puzzleModel | zoom = max (puzzleModel.zoom - 1) 0 }

                            _ ->
                                puzzleModel

                    else
                        puzzleModel
            in
            ( { model | computer = updatedComputer, puzzleModel = newPuzzleModel }
            , Cmd.none
            )

        -- mouse moved - move pieces
        MouseMove pageX pageY ->
            let
                x =
                    pageX

                -- model.screen.left + pageX
                y =
                    pageY

                -- model.screen.top - pageY
                computer =
                    model.computer

                updatedMouse =
                    C.mouseMove x y model.computer.mouse

                updatedComputer =
                    { computer | mouse = updatedMouse }

                puzzleModel =
                    model.puzzleModel

                selection =
                    puzzleModel.selection

                newPuzzleModel =
                    if selection == -1 then
                        puzzleModel

                    else
                        updatePos updatedComputer puzzleModel
            in
            ( { model | computer = updatedComputer, puzzleModel = newPuzzleModel }
            , Cmd.none
            )

        -- mouse button up/down - update selection
        MouseButton isDown ->
            let
                computer =
                    model.computer

                updatedMouse =
                    C.mouseDown isDown computer.mouse

                updatedComputer =
                    { computer | mouse = updatedMouse }

                puzzleModel =
                    model.puzzleModel

                newPuzzleModel =
                    case isDown of
                        True ->
                            updateSelection updatedComputer puzzleModel

                        False ->
                            resetSelection puzzleModel
            in
            ( { model | computer = updatedComputer, puzzleModel = newPuzzleModel }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ E.onResize <| \width height -> WindowResized width height
        , E.onKeyUp (D.map (KeyChanged False) (D.field "key" D.string))
        , E.onKeyDown (D.map (KeyChanged True) (D.field "key" D.string))
        , E.onMouseDown (D.succeed (MouseButton True))
        , E.onMouseUp (D.succeed (MouseButton False))
        , E.onMouseMove (D.map2 MouseMove (D.field "pageX" D.float) (D.field "pageY" D.float))
        ]



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = titleText
    , body = [ viewBody model ]
    }



-- viewBody : Model -> Html msg
-- viewBody model =
--     let
--         posText = "mouse: " ++ (String.fromFloat model.computer.mouse.x) ++ ", " ++ (String.fromFloat model.computer.mouse.y)
--         mouseText = ", mouse down: " ++ (if model.computer.mouse.down then "yes" else "no")
--         selText = ", sel: " ++ (String.fromInt model.puzzleModel.selection)
--         (x, y) = model.puzzleModel.selectedAt
--         selAtText = ", at: " ++ (String.fromFloat x) ++ ", " ++ (String.fromFloat y)
--         zoomText = ", zoom: " ++ (String.fromFloat model.puzzleModel.zoom)
--         txt = posText ++ mouseText ++ selText ++ selAtText ++ zoomText
--     in
--     layout
--         [ width fill
--         , height fill
--         , Background.color color.black
--         , Font.color color.white
--         ]
--        <| column [] [ (text txt), viewPuzzle 0 0 model.computer model.puzzleModel ]
--
-- viewPuzzle : Int -> Int -> C.Computer -> PuzzleModel -> Element msg
-- viewPuzzle x0 y0 computer model =
--     column
--         [ width fill
--         , height fill
--         ]
--         <| pieces model
-- pieces : PuzzleModel -> List (Element msg)
-- pieces model =
--     let
--         url i = "../public/img/" ++ (String.fromInt i) ++ ".png"
--         f i position = placeImage model (url i) position
--     in
--         (Array.indexedMap f model.positions) |> Array.toList
-- placeImage : PuzzleModel -> String -> (Float, Float) -> Element msg
-- placeImage model url (x, y) =
--     let
--         (xz, yz) = getPos model (x, y)
--         (w, h) = getScaled model initialImageSize
--         (xc, yc) = (xz - w/2, yz - h/2)
--     in
--         image
--             [ moveRight xc
--             , moveDown yc
--             , width (px (round w))
--             , height (px (round h))
--             ]
--             { src = url
--             , description = url
--             }


viewBody : Model -> Html msg
viewBody model =
    let
        puzzleText =
            "z: zoom in, x: zoom out, drag/drop to move pieces, move around with arrows. Zoom: " ++ String.fromFloat model.puzzleModel.zoom
    in
    div
        [ H.class "container"
        ]
        [ p [ H.class "puzzle-description" ] [ text puzzleText ]
        , div [ H.class "puzzle-container" ] (pieces model.puzzleModel)
        ]


pieces : PuzzleModel -> List (Html msg)
pieces model =
    let
        url i =
            "img/" ++ String.fromInt i ++ ".png"

        f i position =
            placeImage model (url i) position
    in
    Array.indexedMap f model.positions |> Array.toList


placeImage model url ( x, y ) =
    let
        ( xz, yz ) =
            getPos model ( x, y )

        ( w, h ) =
            getScaled model initialImageSize

        ( xc, yc ) =
            ( xz - w / 2, yz - h / 2 )
    in
    div
        [ H.class "puzzle-div"
        , H.style "left" <| inPixels xc
        , H.style "top" <| inPixels yc
        ]
        [ img
            [ H.src url
            , H.class "puzzle-img"
            , H.style "width" <| inPixels w
            , H.style "height" <| inPixels h
            ]
            []
        ]


updateSelection : C.Computer -> PuzzleModel -> PuzzleModel
updateSelection computer model =
    let
        f i x =
            ( i, x )

        positionsWithIndices =
            Array.indexedMap f model.positions
    in
    Array.foldl (updateIndividual computer) model positionsWithIndices


updateIndividual : C.Computer -> ( Int, ( Float, Float ) ) -> PuzzleModel -> PuzzleModel
updateIndividual computer ( i, ( x, y ) ) model =
    if isWithin computer model <| getPos model ( x, y ) then
        let
            ( sx, sy ) =
                getPos model ( x, y )

            dx =
                sx - computer.mouse.x

            dy =
                sy - computer.mouse.y

            selectedAt =
                ( dx, dy )
        in
        { model | selection = i, selectedAt = selectedAt }

    else
        model


isWithin : C.Computer -> PuzzleModel -> ( Float, Float ) -> Bool
isWithin computer model ( x, y ) =
    let
        ( mx, my ) =
            ( computer.mouse.x, computer.mouse.y )

        ( w, h ) =
            getScaled model initialImageSize

        closeInX =
            x - w / 2 <= mx && mx <= x + w / 2

        closeInY =
            y - h / 2 <= my && my <= y + h / 2
    in
    closeInX && closeInY


updatePos : C.Computer -> PuzzleModel -> PuzzleModel
updatePos computer model =
    let
        ( mx, my ) =
            getInversePos model ( computer.mouse.x, computer.mouse.y )

        ( cx, cy ) =
            getInversePos model <| model.selectedAt

        ( x, y ) =
            Maybe.withDefault ( -100, -100 ) <| Array.get model.selection model.positions

        -- if selection is not a valid index, nothing happens!
        positions =
            Array.set model.selection ( mx + cx, my + cy ) model.positions
    in
    if isWithin computer model <| getPos model ( x, y ) then
        { model | positions = positions }

    else
        model


updateZoom : C.Computer -> PuzzleModel -> PuzzleModel
updateZoom computer model =
    if Set.member "z" computer.keyboard.keys then
        { model | zoom = min (model.zoom + 1) 1000 }

    else if Set.member "x" computer.keyboard.keys then
        { model | zoom = max (model.zoom - 1) 0 }

    else
        model


resetSelection : PuzzleModel -> PuzzleModel
resetSelection model =
    { model | selection = -1 }



-- COORDINATE HELPERS


getScaled model ( x, y ) =
    let
        factor =
            zoomFactor model.zoom
    in
    ( factor * x, factor * y )


getPos model ( x, y ) =
    let
        factor =
            zoomFactor model.zoom

        ( xc, yc ) =
            model.center
    in
    ( factor * x - xc, factor * y - yc )


getInversePos model ( x, y ) =
    let
        factor =
            zoomFactor model.zoom

        ( xc, yc ) =
            model.center
    in
    ( (x + xc) / factor, (y + yc) / factor )


zoomFactor zoom =
    1 + 0.05 * abs zoom


inPixels x =
    String.fromFloat x ++ "px"



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }
