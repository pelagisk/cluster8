module Texts exposing (..)


archive =
    let
        a =
            "Here, we will build a digital exposition of the Sandberg archive!"

        lnk i =
            let
                before =
                    """<img alt="Archive image of moss" src="https://cluster8.org/img/sandberg"""

                mid =
                    String.fromInt i

                after =
                    """.jpg" width="300px">"""
            in
            before ++ mid ++ after
    in
    List.foldr (++) a (List.map lnk <| List.range 0 51)


exhibition =
    """
# Exhibition at Evolutionsmuseet
## [Evolutionsmuseet](http://www.evolutionsmuseet.uu.se/) in Uppsala

From the 12th of June, we are exhibiting at the museum of Evolution in Uppsala.

---

# A point in Time
## Galleri Nos
## 4-5 September 2021
    """



-- exhibition =
--     """
--
-- # A POINT IN TIME
--
-- <p>
-- Adress: Galleri NOS, Kaggeholmsvägen 39, 122 60, Enskede <br />
-- Metro: Tallkrogen <br />
-- Vernissage: Friday September 3, 17.00-21.00 <br />
-- </p>
--
-- Opening hours:
-- - Saturday September 4, 12.00-18.00
-- - Sunday September 5, 12.00-17.00
-- - Talk: Sunday September 5, 15.00
--
-- Pollen holds the potential for a beginning. For life to blossom, and to be carried away to new places. With this exhibition, we embrace the potential for new directions, for the unexpected.
--
-- From the beginning of our collaboration, Cluster 8 has been entangled with the personal archive of Gustav Sandberg, a scientist working at the environmental research station in Abisko between the late 1920s and the late 1970s. Treading the line between speculative invention and discovery, we lived alongside and delved together into this archive, attempting to decipher Sandberg’s organisational systems.
--
-- We became carers, explorers, inventors, code breakers.
--
-- Inspired by rhizomatic thinking, we let our inspirations soar: from palynology to data exploration, to ruins, to trash, to unfathomable numbers scribbled on yellowing bits of paper.
--
-- This exhibition brings us back to Sandberg’s archive. It is our way of giving a physical shape to our research, as well as an opportunity to take it into unforeseen territories. Cluster 8 invites you to be a part of the continuation of our process, to explore the entanglements of interdisciplinary endeavours together.
--
-- Members of Cluster 8 will be on site at all times and we encourage you to come talk to us, ask us questions, challenge us. Our collaboration has been nurtured and strengthened through the passionate sharing of ideas, and we wish to continue this cross-pollination with all of you.
--
-- # GALLERI NOS
--
-- Galleri NOS is an artist-run initiative founded in 2016 by a group of newly graduated artists. The venue is a former daycare center for dogs, which gave the inspiration to the gallery’s name NOS (Swedish for “snout”) and now hosts a gallery as well as studio spaces.
-- The perceived lack of fun, experimental and accessible platforms in Stockholm was the incentive to create a place where people can meet, discuss and view art by inviting non established artists, art students and those who are newly graduated, to exhibit at their venue in one of Stockholms southern suburbs.
-- Galleri NOS is currently run by seven artists.
--
-- [gallerinos.com](https://gallerinos.com/)
--
-- [@galleri_nos](https://www.instagram.com/galleri_nos/)
--
-- """


zine =
    ""


writing =
    ""


palynology =
    ""


ruins =
    ""


about =
    """

# About Cluster 8

Cluster 8 is an interdisciplinary art collective of curatorial practice, art history, quantum physics, and artistic creation. Using experimentation as a form, Cluster 8 explores the borders where art and research meets: between technology and nature, digital process and analog work. The collective consists of

- Isabelle Ribe, curator
- Sara Ekholm Eriksson, artist
- Lior Nønne Malue Hansen, artist
- Axel Gagge, artist and researcher in theoretical quantum physics
- Otto Ruin, art critic and writer

Instagram: [@cluster.8](https://www.instagram.com/cluster.8/)

---

# Exhibition at [Evolutionsmuseet](http://www.evolutionsmuseet.uu.se/) in Uppsala

From the 12th of June, we are exhibiting at the museum of Evolution in Uppsala. More information to come!

---

# Earlier exhibitions

- A point in Time, [Galleri Nos](https://gallerinos.com/), 4-5 September 2021

"""


otto =
    """

When we received the peculiar archive of the botanist Gustav Sandberg, a collection made up of notes, plants, seeds, earth samples, lab reports, it revealed a person who saved and analysed all his scientific impressions. In its thoughtful and almost pedantic way, made up of several boxes within boxes, tubes and folders, the archive was seemingly without any overarching system for the beholder. An archive without centre or periphery.

But after a long engagement with the archive we found a potential trace. A folder, unnamed yet well systematized like the rest of the folders with a recurrent concept, Pollenanalys. The purpose of Pollen Analysis - Palynology, literally the study of dust, is to examine the origin and condition of life which produced the pollen. The technique is able to give valuable information for several disciplines, ranging from geology, paleontology to botany about the causes of climate changes during the quaternary geological epoch as the influence of human life. The pollen’s outer layer, the exine, is incredibly resistant to decay and protected in the stratum sediments it remains well preserved both as living and in fossil form. Spread out by rain and wind, the palynologist collects the particles by extracting samples from layers of sediment in nature and water using a small drill which are then examined under a microscope to identify the types of pollen.

Well folded, Sandberg had saved an article named Fossil forest maps from Dagens Nyheter, Monday August 21st 1933, where the father of modern Palynology, the Swedish geologist Lennart von Post was praised enthusiastically by the famous botanist Rutger Sernader. Decorated with several romantic metaphors are the poetic dimensions of the method indeed many. The pollen which travels from stamen to the pistil by air or insects bears within itself a grain of life. All the pollen and dust in nature is thus a witness of something that has already been while also still waiting to become. In Verdandi småskrifter, Lennart von Post explained the importance of these microscopic particles “Och det är just dessa frömjölskorn, som skola giva oss det sökta skogshistoriska vetandet.” (1932, p.24) The pollen becomes not only a metaphor of historical hope of alternative ways of being but a microhistorical study to explain larger movements of history. From a palynological perspective, the forest is an historical archive of opportunities to explore both individual destinies and larger movements of the past. Through the uneven pollen map history appears not as great or distant but as present; a thin layer of varnish spread out in the spatial space, not visible but yet all around us.

It seemed to us that Palynology was a key concept for Sandberg and inspired by him, we decided to become palynologists as a way of thinking and investigating ruins, their architecture, lost history and as sites of memory.

With help of the data point cloud technique, we can explore further the feelings evoked by these spots like Palynologists. The technique opens up a multidimensional understanding of the ruin – we perceive it as a single totality and simultaneously as a multitude of grains. Sara Ekholm Eriksson used this tool in her film A pixel of a ruin (2021) of the Haga Slottsruin, a site which bears a crucial similarity with pollen particles studied by Palynologists – it never came to life. I will hence visit the site through her work with this method in mind.

<div>
  <iframe src="https://player.vimeo.com/video/530718725" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
</div>

Floating in outer space, the grains of data points create a vision of the architecture while the camera slowly moves up the hill to the ruin. Seeing Ekholm Eriksson’s photo shoots we can almost imagine it as a ruin of a ruin. A fragmentation of the past, where the memory unsuccessfully tries to store and collect knowledge and information of it. Moving around in Ekholm Eriksson’s film, the information of the data points reminds of the ruin’s life cycle, from being architecture to becoming dust and crumble into nature. Or rather, the whole life cycle of the ruin’s temporal process from architecture to dust is here captured in still photo frames.

Trying to grasp and collect as much as possible it revokes Walter Benjamin's most famous Theses on the Philosophy of History where he describes how the Angel of history (A figure inspired by Paul Klee’s Angelus Novus) with his back turned to the future, beholds how history becomes ruins stacked on piles before him while a storm forces him forward, the storm of progress. No matter how much we try to collect and sample, to reawaken the dead, a force drives us forward leaving everything behind in dust. Up until the final scene in A pixel of a ruin the beholder moves around in the quiet space of the ruin through the camera as an open lense. Suddenly without a warning, the spatial space collapses in front of the beholder and the form of the architecture transforms to a cloud of dust and disappears. History becomes not what we collect, rather the opposite - everything that falls out of our hands, memories becoming dust by the forces of time.

In the opening shots of the isolated floating Haga ruin, we perceive human figures moving around. With the palynologist point-cloud technique we perceive them as dissolved structures,  where they seem to almost escape themselves. The pollen structure of the bodies is hanging in the air about to spread out and interlink with the architectural structure of the ruin. The interrelation between pollen ruin and pollen figures creates this strange metamorphosis, where it is impossible to distinguish one from the other. This could be understood as a metaphor of memento mori - human bodies as architecture will both be dust and return to the soil once again indistinguishable - yet, we can also perceive it as the ruin becoming a part of us. Escaping ourselves is thus as much becoming part and integrating with other stories and histories, to incessantly try to transform. Hence the final scene of Ekholm Eriksson’s film which I wrote above leaves the beholder in unease, could be interpreted differently from what I just suggested; not as a collapse of memory and space nor an actual end, but as a pollen rain following us out like the figures in her opening scene. The palynologist reminds us to acknowledge history not as something behind or far distant but sweeping around in the present. Like the small grains of pollen seem to follow us wherever we go during springtime; whether it is an allergic reaction, dust on your clothes or in the gutter after a rainy day, the acts and possibilities of yesterday glows as a yellow shimmer.

"""


axel =
    """

I like to think of “data” as a building block, a contemporary construction material. Data consists of “data points”, information which someone has collected and ordered into a meaningful whole. The ordering of data is a creative and intellectual search for structure. It involves both mathematical methods and artistic visualization. Sometimes, it is called data exploration. In almost every criminal drama, there is a scene of the cops finally finding the crucial piece of evidence in their databases.

We created many 3D scans during our field trips. In digital 3D models, data takes the form of “point clouds”, where each data point represents a physical position in space. The points are connected into triangles to form surfaces which are called “meshes”. In a sense, a 3D modeller “sees” the world as just a set of points connected into triangles. A reductionist but convenient perspective, since it lets us share, combine and change 3D shapes however we want.

This way of seeing is also the characteristic perspective of the data scientist. We might be working with atoms, cells, statistics or currency, but we only see data points. What data is and is not collected? How do I order my data and what is visible in that order? Data scientists struggle to understand their own perspective, to see what they cannot see. It is only this ongoing process which merits the name “scientific”. Humans have a long history of being limited by our own assumptions about the world, and I believe we have a long way to go before we understand the assumptions behind data.

Buildings are abandoned and turn into ruins: plants and animals soon reclaim the sites. In the same way, collections of data are already abandoned, their order forgotten. This was at the back of my mind when we visited the beautiful ruins of Sigtuna, Haga castle and Kymlinge. In the crude 3D scans we collected there, I began to appreciate data as ruin and vice versa.

"""


isabelle =
    """

While spending time with the Gustav Sandberg archive, I developed a visceral awareness that I am, for once, allowed to touch historical remnants of someone else’s life with my bare hands. I had previously conceived of the archive as something inherently experienced through either a glass display case or hypoallergenic latex gloves. It had this aura of the unattainable, reflecting a frenetic human need to preserve as much as possible - even if it means we won’t really be able to get close for fear of destroying it.
During my past experiences in archives, it felt like I was trespassing, looking for the piece of information I needed and leaving immediately. In Sandberg’s archive however, I felt invited as a co-custodian who could take her time with the material.
There is now a layer of myself added to this archive. I left traces behind, from my fingerprints to the personal notes I took. While there is great beauty and knowledge to be found in these new layers of interpretation, this process of inscription through research also begs the following questions: What makes me worthy to leave these traces of myself on someone else’s life? Am I erasing Sandberg’s agency by superimposing my own onto his research?

Reflecting on this layering effect upon the archive as it moves into the care of different people, I was reminded of a talk by Fiona Mackenzie, the archivist at Canna House in the Scottish Hebrides.
Canna House was the house of Margaret Fay Shaw and her husband John Lorne Campbell. They were fascinated by folklore and language, and gathered an immense amount of material on Hebridean culture. Margaret Fay Shaw grew up in New York with a keen interest in music, but she became fascinated by Scottish culture during a visit. In 1929 she decided to settle permanently in South Uist, and she spent her life manually transcribing Gaelic folk songs around the Hebrides.
When the Campbells died, a woman named Magda Sagarzazu became the caretaker of their house. Magda was from the Basque country, and had spent her teen years with the Campbells. She devoted her life to a lifestyle and language that weren’t her own, and her Spanish heritage became completely integral to Canna House. Fiona Mackenzie talked about finding notes and labels in Spanish around the house when she took over from Magda, and she learned Spanish to be able to fully understand the archive. The layering of Gaelic, English and Spanish enriched the house immensely, as each language contains ways of thinking that cannot be fully translated into another language.
Fiona passionately insisted on the importance of recording lives, as folklore is an ongoing process. As a musician herself, she has been continuing to gather more recent stories and music from the Hebrides, and she sang some folk songs for us, keeping the archive alive through the vibrations of her voice. She was convinced that an archive is useless if you don’t do anything with it. Why keep it if no one can experience it?

I thought of conceptual artist Ilya Kabakov’s 1977 text “The Man Who Never Threw Anything Away”. The narrator tells the story of a mysterious plumber who lived in his family’s apartment and hoarded an immense amount of “stuff” in his room. In a manuscript titled “Garbage”, the plumber argues the following:

“To deprive ourselves of these paper symbols and testimonies is to deprive ourselves somewhat of our memories. In our memory everything becomes equally valuable and significant. All points of our recollections are tied to one another. They form chains and connections in our memory which ultimately comprise the story of our life.
To deprive ourselves of all this means to part with who we were in the past, and in a certain sense, it means to cease to exist.” (Kabakov, 1977, p. 33)

I find it interesting to describe the archival drive as such a fierce attempt to continue existing beyond our own ephemerality. But what really resonated with me was the description of the interconnection and lack of strict hierarchy among our memories. During our exploration of the Sandberg archive, we were constantly trying to find a system or crack a code, but we realised that we couldn’t actually judge what was important; everything seemed equally interesting and unfathomable.
Kabakov’s plumber goes on to argue that “grouped together, bound in folders, these papers comprise the single uninterrupted fabric in an entire life, the way it was in the past and the way it is now.” (1977, p. 34). This created an interesting contradiction to Foucault’s description of the archive in “The Historical a priori and the Archive” as something that “deprives us of our continuities” (1969, p. 30),  reflecting a more pessimistic (realistic?) way of looking at an entity that is, in its essence, composed of fragments.
A second manuscript by the plumber, called “A Dump”, explores the idea of the archive as trash, but without the usual disdain directed at unwanted things:

“The whole world, everything which surrounds me here, is to me a boundless dump with no ends or borders, an inexhaustible, diverse sea of garbage. In this refuse of an enormous city one can feel the powerful breathing of its entire past. This whole dump is full of twinkling stars, reflections and fragments of culture.” (Kabakov, 1977, p. 35)

It is fascinating that Kabakov creates a poetic vision of what we discard. Our whole human existence is eventually abandoned, forgotten, decomposed. The concept of the dump flattens hierarchies, and allows for finding value in all things, no matter how lowly they seem. The Sandberg archive could appear to be some random scientist’s trash at first, but if we spend proper time with these records, striving to understand them, the poetics appear out of the garbage.


### Bibliography:

- Foucault, M. (1969). *The Historical a priori and the Archive.* In C. Merewether (Ed.). The Archive (pp. 26-30). London: Whitechapel Gallery.

- Kabakov, I. (1977). *The Man Who Never Threw Anything Away.* In C. Merewether (Ed.). The Archive (pp. 32-37). London: Whitechapel Gallery.

"""


lior =
    """

We opened box upon box

Of culturally wrapped natural material

Allowing the sediments

Of your fragmented collection

To pollenize the ruin in the make

Of our (con)temporary hive mind

We layed out all of our perceptions

On a thousand broken plateaus

In an effort to connect the dots

We puzzled them into an imagery

That felt like a bad compromise

With Deleuze as our tour guide

The rhizome unfolded

An incomplete data set

Set out to date

A point in time forever lost


"""
